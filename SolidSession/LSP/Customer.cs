﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.LSP
{
    public class Customer : IHaveDiscount, ISaveData
    {
        public void Add()
        {
            Console.WriteLine("I save data to db");
        }

        public decimal GetDiscount()
        {
            return 5.0m;
        }
    }
}
