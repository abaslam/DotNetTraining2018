﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.DIP
{
    public class Customer
    {
        private ILogger logger;

        public Customer(ILogger logger)
        {
            this.logger = logger;
        }

        public void Add()
        {
            try
            {
                // Some code to save customer to db
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }

        public void Update()
        {
            try
            {
                // Some code to save customer to db
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }
    }
}
