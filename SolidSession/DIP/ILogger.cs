﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.DIP
{
    public interface ILogger
    {
        void Log(Exception exception);
    }
}
