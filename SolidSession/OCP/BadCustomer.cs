﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.OCP
{
    public class BadCustomer
    {
        private int customerType;

        public BadCustomer(int customerType)
        {

        }

        public decimal GetDiscount()
        {
            if (this.customerType == 1)
            {
                return 5.0m;
            }

            if (this.customerType == 2)
            {
                return 7.5m;
            }

            if (this.customerType == 3)
            {
                return 10.0m;
            }

            return 15.0m;
        }
    }
}
