﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.OCP
{
    public class DiamongCustomer : BaseCustomer
    {
        public override decimal GetDiscount()
        {
            return 10.0m;
        }
    }
}
