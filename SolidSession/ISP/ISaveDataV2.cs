﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLIDSession.ISP
{
    public interface ISaveDataV2 : ISaveDataV1
    {
        void Add(bool commitChanges);
    }
}
