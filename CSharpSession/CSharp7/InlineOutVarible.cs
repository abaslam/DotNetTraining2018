﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures.CSharp7
{
    public class InlineOutVarible
    {
        public static void OutVariable()
        {
            string value = "10001";
            int numericValue;
            if(int.TryParse(value, out numericValue))
            {
                Console.WriteLine("Parse success");
            }
            else
            {
                Console.WriteLine("Parse failed");
            }

            
        }

        public static void InlineOutVariable()
        {
            string value = "10001";
            if (int.TryParse(value, out int numericValue))
            {
                Console.WriteLine("Parse success");
            }
            else
            {
                Console.WriteLine("Parse failed");
            }

            if (int.TryParse(value, out var numericValue2))
            {
                Console.WriteLine("Parse success");
            }
            else
            {
                Console.WriteLine("Parse failed");
            }
        }
    }
}
