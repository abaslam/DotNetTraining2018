﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures.CSharp7
{
    public class ValuesTuples
    {
        public static void GenericTuples() // Older feature
        {
            var genericTuples = new Tuple<int, string, int, int, int, int, int, int>(1, 2.ToString(), 3, 4, 5, 6, 7, 8);
                        
            Console.WriteLine(genericTuples.Item1);
        }

        public static void ValueTuplesAssigning()
        {
            (string Name, int age) person = ("Some one", 10);
            
            Console.WriteLine(person.Name);

            person.Name = "New Name";
        }

        public static void ValueTupleInMethod()
        {
            (int, int, int) time = GetTime();
            time.Item1 = 10;

            var time1 = GetTime();
            time1.Item1 = 20;

            var newTime = GetTimeWithTupleName();
            newTime.Hour = 24;
        }

        public static void ValueTupleWithReferenceType()
        {
            var customer = new Customer();
            var order = new Order();
            var orderDetails = new List<OrderDetail>();

            var orderDto = (customer, order, orderDetails, 1);            
        }

        public static (int, int, int) GetTime()
        {
            return (1, 30, 40);
        }

        public static (int Hour, int Minute, int Seconds) GetTimeWithTupleName()
        {
            return (1, 30, 40);
        }

        private class Order
        {
            public int OrderId { get; set; }
            public int Customer { get; set; }
        }

        private class Customer
        {
            public int CustomerId { get; set; }
            public string CustomerName { get; set; }
        }

        private class OrderDetail
        {
            public int OrderDetailId { get; set; }
            public int Quantity { get; set; }
            public decimal UnitPrice { get; set; }
        }
    }
}
