﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures.CSharp7
{
    public class PrivateFunctions
    {
        public static void WithoutPrivateFunction()
        {
            Log("Starting");
            var some = "What";
            Log("Calculating");
            
        }

        public static void WithPrivateFunction()
        {
            Log("Ha ha");
            //void LogPrivate(string message)
            //{
            //    LogNestedPrivate(message);
            //    void LogNestedPrivate(string message1)
            //    {
            //        Console.WriteLine(message1);
            //    }
            //}

          
            LogPrivate("Starting");
            
            var another = "What";
            LogPrivate("Calculating");
            var spaces = GetSpace(10);

            void LogPrivate(string message)
            {
                Console.WriteLine(message);
            }

            string GetSpace(int value)
            {
                return "";
            }            
        }

        private static void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
