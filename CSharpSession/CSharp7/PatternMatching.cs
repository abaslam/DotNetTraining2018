﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpNewFeatures.CSharp7
{
    public class PatternMatching
    {
        public static void SampleWithoutPatternMatching()
        {
            var someList = new List<object>();

            var violation1 = new ViolationStatus { Id = 1, IsFatal = false };
            var violation2 = new ViolationStatus { Id = 2, IsFatal = true };

            var health1 = new HealthStatus { Id = 1, IsHealth = true, Name = "Some Name" };
            var health2 = new HealthStatus { Id = 2, IsHealth = false, Name = "Other Name" };

            someList.Add(violation1);
            someList.Add(violation2);
            someList.Add(health1);
            someList.Add(health2);

            foreach (var item in someList)
            {
                var healthStatus = (HealthStatus)item;

                if (healthStatus != null)
                {
                    if (healthStatus.IsHealth)
                    {
                        healthStatus.Name = "Healthy" + " " + healthStatus.Name;
                    }
                    else
                    {
                        healthStatus.Name = "Un healthy" + " " + healthStatus.Name;
                    }

                    continue;
                }

                var violationStatus = (ViolationStatus)item;
                if (violationStatus != null)
                {
                    if (violationStatus.IsFatal)
                    {
                        ///
                    }
                    else
                    {

                    }
                }
            }
        }

        public static void SampleWithPatternMatching()
        {
            var someList = new List<object>();

            var violation1 = new ViolationStatus { Id = 1, IsFatal = false };
            var violation2 = new ViolationStatus { Id = 2, IsFatal = true };

            var health1 = new HealthStatus { Id = 1, IsHealth = true, Name = "Some Name" };
            var health2 = new HealthStatus { Id = 2, IsHealth = false, Name = "Other Name" };

            someList.Add(violation1);
            someList.Add(violation2);
            someList.Add(health1);
            someList.Add(health2);

            foreach (var item in someList)
            {    
                if(item is HealthStatus healthStatus)
                {
                    if (healthStatus.IsHealth)
                    {
                        healthStatus.Name = "Healthy" + " " + healthStatus.Name;
                    }
                    else
                    {
                        healthStatus.Name = "Un healthy" + " " + healthStatus.Name;
                    }
                }
                else if(item is ViolationStatus violationStatus && !violationStatus.IsFatal)
                {
                    //
                } 
            }
        }

        public static void PatternMatchingInSwitchStatement()
        {
            var employees = new List<Employee>();
            Employee employee = new VicePresident();
            employee.Salary = 175000;
            employee.Years = 7;
            (employee as VicePresident).NumberManaged = 200;
            (employee as VicePresident).StockShares = 6000;

            employees.Add(employee);

            foreach (var employee1 in employees)
            {
                switch (employee1)
                {
                    case VicePresident vp when (vp.StockShares < 5000):
                        Console.WriteLine($"Junior VP with {vp.StockShares}");
                        break;

                    case VicePresident vp when (vp.StockShares >= 5000):
                        Console.WriteLine($"Senior VP with {vp.StockShares}");
                        break;

                    case Manager m:
                        Console.WriteLine($"Number of people managed {m.NumberManaged}");
                        break;

                    case Employee e:
                        Console.WriteLine($"Number of year in Company {e.Years}");
                        break;
                }
            }
        }

        private class ViolationStatus
        {
            public int Id { get; set; }
            public bool IsFatal { get; set; }
        }

        private class HealthStatus
        {
            public int Id { get; set; }
            public bool IsHealth { get; set; }
            public string Name { get; set; }
        }

        public class Employee
        {
            public int Salary { get; set; }
            public int Years { get; set; }
        }

        public class Manager : Employee
        {
            public int NumberManaged { get; set; }
        }

        public class VicePresident : Manager
        {
            public int StockShares { get; set; }
        }
    }
}
