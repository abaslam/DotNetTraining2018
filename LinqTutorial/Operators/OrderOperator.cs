﻿using LinqSession.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSession.Operators
{
    public static class OrderOperator
    {
        public static void Simple()
        {
            using (var db = new Northwind())
            {
                var sortedProducts = from p in db.Products
                                     orderby p.ProductName
                                     select p.ProductName;

                foreach (var item in sortedProducts)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }
        }

        public static void SimpleFluent()
        {
            using (var db = new Northwind())
            {
                var sortedProducts = db.Products.OrderBy(x => x.ProductName).Select(x => x.ProductName);

                foreach (var item in sortedProducts)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }
        }

        public static void SimpleDescending()
        {
            using (var db = new Northwind())
            {
                var sortedProducts = from p in db.Products
                                     orderby p.UnitsInStock descending
                                     select p.ProductName;

                foreach (var item in sortedProducts)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }
        }

        public static void SimpleDescendingFluent()
        {

            using (var db = new Northwind())
            {
                var sortedProducts = db.Products.OrderByDescending(x => x.ProductName);

                if (true)
                {
                    sortedProducts = sortedProducts.ThenBy(x => x.CategoryID);
                }

                foreach (var item in sortedProducts)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }

        }

        public static void ThenBy()
        {
            using (var db = new Northwind())
            {
                var sortedProducts = (from p in db.Products
                                      orderby p.Category.CategoryName, p.UnitPrice descending
                                      select p);

                foreach (var item in sortedProducts)
                {
                    Console.WriteLine("Name : {0}", item.ProductName);
                }
            }
        }

        public static void ThenByFluent()
        {

            using (var db = new Northwind())
            {
                var sortedProducts = db.Products.OrderByDescending(x => x.ProductName).ThenBy(x => x.SupplierID).Select(x => x.ProductName);

                foreach (var item in sortedProducts)
                {
                    Console.WriteLine("Name : {0}", item);
                }
            }

        }
    }
}
