﻿using LinqSession.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSession.Operators
{
    public static class GroupByOperator
    {
        public static void Simple()
        {
            using (var db = new Northwind())
            {
                var groupByCategory = from product in db.Products
                                      group product by product.CategoryID into groups
                                      select new { CategoryId = groups.Key, Products = groups.Select(x => x) };


                foreach (var group in groupByCategory)
                {
                    Console.WriteLine("Order {0}: {1}", group.CategoryId, group.Products.Count());

                    foreach (var product in group.Products)
                    {
                        Console.WriteLine("Product {0}: {1}", product.ProductName, product.ReorderLevel);
                    }
                }
            }
        }

        public static void SimpleFluent()
        {
            using (var db = new Northwind())
            {
                var groupByCategory = db.Products
                                        .GroupBy(x => x.CategoryID)
                                        .Select(x => new
                                        {
                                            CategoryId = x.Key,
                                            Products = x.Select(y => new
                                            {
                                                y.ProductName,
                                                y.ReorderLevel
                                            })
                                        });

                foreach (var group in groupByCategory)
                {
                    Console.WriteLine("Order {0}: {1}", group.CategoryId, group.Products.Count());

                    foreach (var product in group.Products)
                    {
                        Console.WriteLine("Product {0}: {1}", product.ProductName, product.ReorderLevel);
                    }
                }
            }
        }

        public static void Multiple()
        {
            using (var db = new Northwind())
            {
                var groupByCategory = db.Products.GroupBy(x => new { x.CategoryID, x.Supplier.ContactName }).Select(x => new
                {
                    CategoryId = x.Key.CategoryID,
                    SupplierName = x.Key.ContactName,
                    Products = x.Select(y => y)
                });

                foreach (var group in groupByCategory)
                {
                    Console.WriteLine("Category {0} : {1} : {2}", group.CategoryId, group.SupplierName, group.Products.Count());
                }
            }
        }

        public static void SqlFunction()
        {
            using (var db = new Northwind())
            {
                var order = db.Orders.Where(x => SqlFunctions.DateDiff("mm", x.OrderDate.Value, DateTime.Now) > 10).ToList();

                Console.WriteLine(order.Count);
            }

        }

        public static void Nested()
        {
            using (var db = new Northwind())
            {
                var customerOrderGroups = from customer in db.Customers
                                          select
                                              new
                                              {
                                                  customer.CompanyName,
                                                  YearGroups =
                                                      from o in customer.Orders
                                                      //group o by o.OrderDate.Value.Year into yg
                                                      group o by SqlFunctions.DatePart("year", o.OrderDate) into yg
                                                      select
                                                          new
                                                          {
                                                              Year = yg.Key,
                                                              MonthGroups =
                                                                  from o in yg
                                                                  group o by SqlFunctions.DatePart("month", o.OrderDate) into mg
                                                                  select new { Month = mg.Key, Orders = mg }
                                                          }
                                              };

                foreach (var item in customerOrderGroups)
                {
                    Console.WriteLine("Company : {0}", item.CompanyName);

                    foreach (var years in item.YearGroups)
                    {
                        Console.WriteLine("Year : {0}", years.Year);

                        foreach (var month in years.MonthGroups)
                        {
                            Console.WriteLine("Month : {0} Order : {1}", month.Month, month.Orders.Count());

                            foreach (var order in month.Orders)
                            {
                                Console.WriteLine("Order Date :{0}", order.OrderDate);
                            }
                        }
                    }
                }
            }
        }



        public static void NestedWithoutSqlFunction()
        {
            using (var db = new Northwind())
            {
                var customerOrderGroups = from customer in db.Customers
                                          select
                                              new
                                              {
                                                  customer.CompanyName,
                                                  YearGroups =
                                                      from o in customer.Orders
                                                      group o by o.OrderDate.Value.Year into yg
                                                      select
                                                          new
                                                          {
                                                              Year = yg.Key,
                                                              MonthGroups =
                                                                  from o in yg
                                                                  group o by o.OrderDate.Value.Month into mg
                                                                  select new { Month = mg.Key, Orders = mg }
                                                          }
                                              };

                foreach (var item in customerOrderGroups)
                {
                    Console.WriteLine("Company : {0}", item.CompanyName);

                    foreach (var years in item.YearGroups)
                    {
                        Console.WriteLine("Year : {0}", years.Year);

                        foreach (var month in years.MonthGroups)
                        {
                            Console.WriteLine("Month : {0} Order : {1}", month.Month, month.Orders.Count());

                            foreach (var order in month.Orders)
                            {
                                Console.WriteLine("Order Date :{0}", order.OrderDate);
                            }
                        }
                    }
                }
            }
        }
    }
}
