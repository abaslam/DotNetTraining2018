﻿using LinqSession.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqSession.Operators
{
    public static class PartitionOperator
    {
        public static void Take()
        {
            using (var db = new Northwind())
            {

                var first3WAOrders = (
                            from c in db.Customers
                            from o in c.Orders
                            where c.Region == "WA"
                            select new { c.CustomerID, o.OrderID, o.OrderDate })
                            .Take(3);

                foreach (var order in first3WAOrders)
                {
                    Console.WriteLine("Order {0}: {1}", order.CustomerID, order.OrderDate);
                }
            }
        }

        public static void Skip()
        {
            using (var db = new Northwind())
            {
                var waOrders = from c in db.Customers
                               from o in c.Orders
                               where c.Region == "WA"
                              orderby c.CustomerID
                               select new { c.CustomerID, o.OrderID, o.OrderDate };

                var allButFirst2Orders = waOrders.Skip(2);

                foreach (var order in allButFirst2Orders)
                {
                    Console.WriteLine("Order {0}: {1}", order.CustomerID, order.OrderDate);
                }

            }
        }

        public static void TakeWhile()
        {
            using (var db = new Northwind())
            {
                var waOrders = from c in db.Customers
                               from o in c.Orders
                               where c.Region == "WA"
                               select new { c.CustomerID, o.OrderID, o.OrderDate };

                var filterDate = DateTime.Parse("6/19/199");
                var allButFirst2Orders = waOrders.AsEnumerable()
                    .SkipWhile(x => x.OrderDate == filterDate);

                foreach (var order in allButFirst2Orders)
                {
                    Console.WriteLine("Order {0}: {1}", order.CustomerID, order.OrderDate);
                }

            }
        }
    }
}
