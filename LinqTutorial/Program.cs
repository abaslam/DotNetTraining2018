﻿using LinqSession.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            WhereOperator.Simple();
            //WhereOperator.DrillDown();
            //WhereOperator.Any();
            //WhereOperator.AnyWithCondition();
            //WhereOperator.AllWithCondition();

            //ProjectionOperators.Simpe();
            //ProjectionOperators.AnonymousType();
            //ProjectionOperators.StronglyTyped();
            //ProjectionOperators.SelectMany();
            //ProjectionOperators.MultiSelectMany();
            //ProjectionOperators.ToDictionary();
            //ProjectionOperators.FirstOrDefault();
            //ProjectionOperators.FirstOrderDefaultWithCondition();

            //PartitionOperator.Take();
            //PartitionOperator.Skip();
            //PartitionOperator.TakeWhile();

            //SetOperator.Distinct();
            //SetOperator.Union();
            //SetOperator.Intersect();
            //SetOperator.Exccept();

            //OrderOperator.Simple();

            //OrderOperator.SimpleFluent();
            //OrderOperator.SimpleDescending();
            //OrderOperator.ThenBy();
            //OrderOperator.ThenByFluent();

            //JoinOperator.InnerJoin();
            //JoinOperator.GroupJoin();
            //JoinOperator.InnerJoinWithGroupJoin();
            //JoinOperator.LeftOuterJoin();

            //GroupByOperator.Simple();            
            //GroupByOperator.SimpleFluent();
            //GroupByOperator.Multiple();
            //GroupByOperator.Nested();
            //GroupByOperator.NestedWithoutSqlFunction();
            //GroupByOperator.SqlFunction();

            Console.ReadLine();
        }
    }
}
