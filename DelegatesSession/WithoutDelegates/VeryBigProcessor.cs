﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesSession.WithoutDelegates
{
    public class VeryBigProcessor
    {
        public void Process()
        {
            Console.WriteLine("Starting Process");
            for (int i = 0; i < 10000; i++)
            {
                Console.WriteLine("Processing - " + i);
            }

            Console.WriteLine("Ending Process");
        }
    }
}
