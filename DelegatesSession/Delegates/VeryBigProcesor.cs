﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesSession.Delegates
{
    //public delegate void NoParamNoReturn();
    //public delegate int NoParam();
    public delegate void Logger(string message);

    public class VeryBigProcesor
    {
        public Logger ProcessorLogger { get; set; }

        public void Process()
        {
            ProcessorLogger("Starting Process");            

            for (int i = 0; i < 10000; i++)
            {
                ProcessorLogger("Processing " + i);
            }

            ProcessorLogger("Ending Process");
        }

        public void ProcessWithParameter(Logger logger)
        {
            logger?.Invoke("Starting Process");

            for (int i = 0; i < 10000; i++)
            {
                logger?.Invoke("Processing " + i);
            }

            logger?.Invoke("Ending Process");
        }
    }
}
