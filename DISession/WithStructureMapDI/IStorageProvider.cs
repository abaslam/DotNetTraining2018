﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDI
{
    public interface IStorageProvider
    {
        StorageTypes StorageType { get; }
        string GetContent(StorageOptions options);
    }
}
