﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDI
{
    public class ColonSperatedParser : IParser
    {
        private readonly IEnumerable<IStorageProvider> storageProviders;

        public ParserTypes ParserType => ParserTypes.ColonSeperated;

        public ColonSperatedParser(IEnumerable<IStorageProvider> storageProviders)
        {
            this.storageProviders = storageProviders;
        }

        public string Parse(StorageOptions options)
        {
            var currentStorageProvider = this.storageProviders.FirstOrDefault(x => x.StorageType == options.StorageType);
            return $"Parsed Succesffull from {currentStorageProvider.GetContent(options)} using ColonSeperated parser";
        }
    }
}
