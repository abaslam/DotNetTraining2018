﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDI
{
    public class FileSystemStorageProvider : IStorageProvider
    {
        public StorageTypes StorageType =>  StorageTypes.FileSystem;

        public string GetContent(StorageOptions options)
        {
            return $"I am returning data from FileSystem with path - {options.Path}";
        }
    }
}
