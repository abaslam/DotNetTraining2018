﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithManualDI
{
    public class ColonSperatedParser : IParser
    {
        private readonly IStorageProvider storageProvider;

        public ParserTypes ParserType => throw new NotImplementedException();

        public ColonSperatedParser(IStorageProvider storageProvider)
        {
            this.storageProvider = storageProvider;
        }

        public string Parse(StorageOptions options)
        {
            return $"Parsed Succesffull from {this.storageProvider.GetContent(options)} using ColonSeperated parser";
        }
    }
}
