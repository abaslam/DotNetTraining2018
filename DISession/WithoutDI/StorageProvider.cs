﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithoutDI
{
    public class StorageProvider
    {
        public string GetContent(StorageOptions options)
        {
            if (options.StorageType == StorageTypes.FileSystem)
            {
                return $"I am returning data from FileSystem with path - {options.Path}";
            }

            if (options.StorageType == StorageTypes.Database)
            {
                return $"I am returning data from Database with path - {options.Path}";
            }

            return $"I am returning data from Azure with path - {options.Path}";
        }
    }
}
