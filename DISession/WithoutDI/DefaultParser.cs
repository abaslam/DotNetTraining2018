﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithoutDI
{
    public class DefaultParser
    {
        private readonly StorageProvider storage;

        public DefaultParser()
        {
            this.storage = new StorageProvider();
        }

        public string Parse(StorageOptions options)
        {
            if (options.ParserType == ParserTypes.CommaSeperated)
            {
                return $"Parsed Succesffull from {this.storage.GetContent(options)} using CommaSeperated parser";
            }

            if (options.ParserType == ParserTypes.TabSeperated)
            {
                return $"Parsed Succesffull from {this.storage.GetContent(options)} using TabSeperated parser";
            }

            return $"Parsed Succesffull from {this.storage.GetContent(options)} using ColonSeperated parser";
        }
    }
}
