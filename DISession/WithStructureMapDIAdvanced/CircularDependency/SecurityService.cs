﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.CircularDependency
{
    public class SecurityService : ISecurityService
    {
        private readonly Func<IAuthenticationService> authenticationServiceFactory;

        public SecurityService(Func<IAuthenticationService> authenticationServiceFactory)
        {
            this.authenticationServiceFactory = authenticationServiceFactory;
        }

        public string GetSession(string userId)
        {
            var authenticationService = this.authenticationServiceFactory();
            return userId;
        }

        public bool IsValidPassword(string userName, string password)
        {
            return true;
        }
    }
}
