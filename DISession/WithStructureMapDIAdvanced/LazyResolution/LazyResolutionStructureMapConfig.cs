﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced
{
    public class LazyResolutionStructureMapConfig
    {
        private static IContainer container = new Container();

        public static IContainer Container => container;

        public static void Configure()
        {
            container.Configure(x =>
            {
                //x.For<IApplicationConfig>().Use<ApplicationConfig>();
                x.For<IApplicationConfig>().Use(() => GetCurrentApplicationConfig());
            });
        }

        private static IApplicationConfig GetCurrentApplicationConfig()
        {
            return new ApplicationConfig { ConnectionString = $"I am the connection String for {SessionStore.CurrentOrganization}" };
        }
    }
}
