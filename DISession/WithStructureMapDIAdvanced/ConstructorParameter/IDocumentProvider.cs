﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.ConstructorParameter
{
    public interface IDocumentProvider
    {
        string GetDocument(string path);
    }
}
