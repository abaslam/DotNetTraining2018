﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.ConstructorParameter
{
    public class FileSystemDocumentProvider : IDocumentProvider
    {
        private readonly string basePath;

        public FileSystemDocumentProvider(string basePath)
        {
            this.basePath = basePath;
        }

        public string GetDocument(string path)
        {
            return $"I am returning file data from {this.basePath}-{path}";
        }
    }
}
