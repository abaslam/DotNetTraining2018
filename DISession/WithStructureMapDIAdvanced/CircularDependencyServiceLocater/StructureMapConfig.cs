﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.CircularDependencyServiceLocater
{
    public class StructureMapConfig
    {
        private static IContainer container = new Container();

        public static IContainer Container => container;

        public static void Configure()
        {
            container.Configure(x =>
            {
                x.For<ISecurityService>().Use<SecurityService>();
                x.For<IAuthenticationService>().Use<AuthenticationService>();
            });
        }
    }
}
