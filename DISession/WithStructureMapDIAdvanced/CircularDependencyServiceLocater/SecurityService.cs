﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.CircularDependencyServiceLocater
{
    public class SecurityService : ISecurityService
    {
        private readonly IContainer container;

        public SecurityService(IContainer container)
        {
            this.container = container;
        }

        public string GetSession(string userId)
        {
            var authenticationService = this.container.GetInstance<IAuthenticationService>();
            return userId;
        }

        public bool IsValidPassword(string userName, string password)
        {
            return true;
        }
    }
}
