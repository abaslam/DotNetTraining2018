﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.CircularDependencyServiceLocater
{
    public interface ISecurityService
    {
        string GetSession(string userId);
        bool IsValidPassword(string userName, string password);
    }
}
