﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession.WithStructureMapDIAdvanced.Generics
{
    public interface IRequesHandler<TRequest>
    {
        bool Handle(TRequest request);
    }
}
