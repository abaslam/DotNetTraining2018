﻿using DISession.WithoutDI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DISession
{
    class Program
    {
        static void Main(string[] args)
        {
            // Without DI
            //var options = new StorageOptions { Path = "I am a very secret path", ParserType= ParserTypes.ColonSeperated, StorageType= StorageTypes.Azure };
            //var parser = new DefaultParser();
            //Console.WriteLine($"Parse result = {parser.Parse(options)}");


            // Manual DI

            //var options = new WithManualDI.StorageOptions { Path = "I am a very secret path", ParserType = WithManualDI.ParserTypes.ColonSeperated, StorageType = WithManualDI.StorageTypes.Azure };
            //WithManualDI.IParser parser= null;
            //WithManualDI.IStorageProvider storageProvider = null;
            //if(options.StorageType== WithManualDI.StorageTypes.FileSystem)
            //{
            //    storageProvider = new WithManualDI.FileSystemStorageProvider();
            //}

            //if(options.StorageType == WithManualDI.StorageTypes.Database)
            //{
            //    storageProvider = new WithManualDI.DatabaseStorageProvider();
            //}

            //if(options.StorageType== WithManualDI.StorageTypes.Azure)
            //{
            //    storageProvider = new WithManualDI.AzureStorageProvider();
            //}

            //if(options.ParserType== WithManualDI.ParserTypes.CommaSeperated)
            //{
            //    parser = new WithManualDI.CommaSeperatedParser(storageProvider);
            //}

            //if (options.ParserType == WithManualDI.ParserTypes.ColonSeperated)
            //{
            //    parser = new WithManualDI.ColonSperatedParser(storageProvider);
            //}

            //if (options.ParserType == WithManualDI.ParserTypes.TabSeperated)
            //{
            //    parser = new WithManualDI.TabSperatedParser(storageProvider);
            //}

            // Manual DI

            //WithStructureMapDI.StructuremapConfig.Configure();

            //var options = new WithStructureMapDI.StorageOptions { Path = "I am a very secret path", ParserType = WithStructureMapDI.ParserTypes.CommaSeperated, StorageType = WithStructureMapDI.StorageTypes.FileSystem };
            //// method0- when only one implementation
            ////var parser = WithStructureMapDI.StructuremapConfig.Container.GetInstance<WithStructureMapDI.IParser>();

            //// method1 - by name - multiple
            ////var parser = WithStructureMapDI.StructuremapConfig.Container.GetInstance<WithStructureMapDI.IParser>(options.ParserType.ToString());

            //// method2 - by collection and linq;
            //var parser = WithStructureMapDI.StructuremapConfig.Container.GetAllInstances<WithStructureMapDI.IParser>().FirstOrDefault(x => x.ParserType == options.ParserType);
            //Console.WriteLine($"Parse result = {parser.Parse(options)}");

            //WithStructureMapDIAdvanced.LazyResolutionStructureMapConfig.Configure();

            //WithStructureMapDIAdvanced.SessionStore.CurrentOrganization = "ECDS";

            //var applicationConfig = WithStructureMapDIAdvanced.LazyResolutionStructureMapConfig.Container.GetInstance<WithStructureMapDIAdvanced.IApplicationConfig>();

            //Console.WriteLine("The connection String " + applicationConfig.ConnectionString);

            //WithStructureMapDIAdvanced.SessionStore.CurrentOrganization = "XYZ";

            //applicationConfig = WithStructureMapDIAdvanced.LazyResolutionStructureMapConfig.Container.GetInstance<WithStructureMapDIAdvanced.IApplicationConfig>();

            //Console.WriteLine("The connection String " + applicationConfig.ConnectionString);

            //WithStructureMapDIAdvanced.ConstructorParameter.StucureMapConfig.Configure();

            //var documentProvider = WithStructureMapDIAdvanced.ConstructorParameter.StucureMapConfig.Container.GetInstance<WithStructureMapDIAdvanced.ConstructorParameter.IDocumentProvider>();

            //Console.WriteLine(documentProvider.GetDocument("Some path"));

            //WithStructureMapDIAdvanced.CircularDependency.StructureMapConfig.Configure();

            //var securityService = WithStructureMapDIAdvanced.CircularDependency.StructureMapConfig.Container.GetInstance<WithStructureMapDIAdvanced.CircularDependency.ISecurityService>();

            //securityService.GetSession("someId");

            //WithStructureMapDIAdvanced.CircularDependencyServiceLocater.StructureMapConfig.Configure();

            //var securityService = WithStructureMapDIAdvanced.CircularDependencyServiceLocater.StructureMapConfig.Container.GetInstance<WithStructureMapDIAdvanced.CircularDependencyServiceLocater.ISecurityService>();

            //securityService.GetSession("someId");

            WithStructureMapDIAdvanced.Generics.StructureMapConfig.Configure();

            var orderPlaced = new WithStructureMapDIAdvanced.Generics.OrderPlacedRequest { OrderId = 100, ProductId = 123123 };

            var orderPlacedHandler = WithStructureMapDIAdvanced.Generics.StructureMapConfig.Container.GetInstance<WithStructureMapDIAdvanced.Generics.IRequesHandler<WithStructureMapDIAdvanced.Generics.OrderPlacedRequest>>();

            orderPlacedHandler.Handle(orderPlaced);

            var orderShipped = new WithStructureMapDIAdvanced.Generics.OrderShippedRequest { OrderId = 100, ShipmentType = 4567 };

            var orderShippedHandler = WithStructureMapDIAdvanced.Generics.StructureMapConfig.Container.GetInstance<WithStructureMapDIAdvanced.Generics.IRequesHandler<WithStructureMapDIAdvanced.Generics.OrderShippedRequest>>();

            orderShippedHandler.Handle(orderShipped);

            Console.ReadLine();
        }
    }
}
