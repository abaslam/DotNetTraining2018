﻿using AutoMapper;
using Automapper2018.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automapper2018.Topics
{
    public class AutoMapperBasics
    {
        public static void SimpleMap()
        {
            Mapper.Initialize(c => c.CreateMap<Order, OrderDTO>());

            var customer = new Customer
            {
                Name = "George Costanza"
            };

            var order = new Order
            {
                Customer = customer
            };
            var bosco = new Product
            {
                Name = "Bosco",
                Price = 4.99m
            };

            order.AddOrderLineItem(bosco, 15);

            var dto = new OrderDTO();

            Mapper.Map(order, dto);

            //var dto = Mapper.Map<OrderDTO>(order);

            //var dto = Mapper.Map<Order, OrderDTO>(order);

            Console.WriteLine("Customer Name: " + dto.CustomerName);
            Console.WriteLine("Orer Total:" + dto.Total);
        }

        public static void MapFrom()
        {
            Mapper.Initialize(c => c.CreateMap<CalendarEvent, CalendarEventForm>()
                                    .ForMember(x => x.EventDate, y => y.MapFrom(z => z.Date))
                                    .ForMember(x => x.EventHour, y => y.MapFrom(z => z.Date.Hour > 24 ? (24 - z.Date.Hour) : z.Date.Hour))
                                    .ForMember(x => x.EventMinute, y => y.MapFrom(z => z.Date.Minute)));

            var calendarEvent = new CalendarEvent
            {
                Date = new DateTime(2008, 12, 15, 20, 30, 0),
                Title = "Company Holiday Party"
            };

            CalendarEventForm form = Mapper.Map<CalendarEvent, CalendarEventForm>(calendarEvent);

            Console.WriteLine("Title :" + form.Title);
            Console.WriteLine("Date :" + form.EventDate);
            Console.WriteLine("Hours :" + form.EventHour);
            Console.WriteLine("Minute :" + form.EventMinute);
        }

        public static void ReverseMap()
        {
            Mapper.Initialize(c =>
            {
                c.CreateMap<Person, PersonDTO>().ReverseMap();
                //c.CreateMap<PersonDTO, Person>();                
            });


            var person = new Person();
            person.FirstName = "John";
            person.LastName = "Doe";

            var personeDTO = Mapper.Map<Person, PersonDTO>(person);

            Console.WriteLine("Full Name: " + personeDTO.FullName);

            var newPerson = Mapper.Map<PersonDTO, Person>(personeDTO);

            Console.WriteLine("Full Name: " + newPerson.FullName);
        }

        public static void Ignore()
        {
            Mapper.Initialize(c =>
            {
                c.CreateMap<Person, PersonDTO>().ReverseMap().ForMember(x => x.VerySecretValue, y => y.Ignore());
                //c.CreateMap<PersonDTO, Person>();                
            });


            var person = new Person();
            person.FirstName = "John";
            person.LastName = "Doe";

            var personeDTO = Mapper.Map<Person, PersonDTO>(person);
            Console.WriteLine("Secret Value " + personeDTO.VerySecretValue);
            Console.WriteLine("Full Name: " + personeDTO.FullName);
            personeDTO.VerySecretValue = Guid.Empty;

            var newPerson = Mapper.Map<PersonDTO, Person>(personeDTO);

            Console.WriteLine("Secret Value " + newPerson.VerySecretValue);
            Console.WriteLine("Full Name: " + newPerson.FullName);
        }

        public static void ConditionalMapping()
        {
            Mapper.Initialize(c =>
            {
                c.CreateMap<Person, PersonDTO>().ReverseMap().ForMember(x => x.VerySecretValue, y => y.Condition(z => z.FirstName == "Very Secret"));
            });


            var person = new Person();
            person.FirstName = "John";
            person.LastName = "Doe";

            var personeDTO = Mapper.Map<Person, PersonDTO>(person);
            Console.WriteLine("Secret Value " + personeDTO.VerySecretValue);
            Console.WriteLine("Full Name: " + personeDTO.FullName);
            personeDTO.FirstName = "Very Secret";
            personeDTO.VerySecretValue = Guid.Empty;

            var newPerson = Mapper.Map<PersonDTO, Person>(personeDTO);

            Console.WriteLine("Secret Value " + newPerson.VerySecretValue);
            Console.WriteLine("Full Name: " + newPerson.FullName);
        }

        public static void GenericMapping()
        {
            Mapper.Initialize(c =>
            {
                c.CreateMap<Person, PersonDTO>();
            });

            var persons = new List<Person>();

            for (int i = 0; i < 10; i++)
            {
                var person = new Person();
                person.FirstName = "Person FName " + i;
                person.LastName = "Person LName " + i;

                persons.Add(person);
            }

            //var personDTOs = new List<PersonDTO>();

            //foreach (var person in persons)
            //{
            //    personDTOs.Add(Mapper.Map<PersonDTO>(person));
            //}

            var personDTOs = Mapper.Map<List<Person>, List<PersonDTO>>(persons);
            //var personDTOs = Mapper.Map<List<Person>, PersonDTO[]>(persons);
            foreach (var personDTO in personDTOs)
            {
                Console.WriteLine("Full Name: " + personDTO.FullName);
            }
        }

        public static void OpenGenericMapping()
        {
            //Mapper.Initialize(c =>
            //{
            //    c.CreateMissingTypeMaps = false;
            //    c.CreateMap<SampleList<int>, SampleListDTO<int>>();
            //    c.CreateMap<SampleList<string>, SampleListDTO<string>>();
            //});

            Mapper.Initialize(c =>
            {
                c.CreateMissingTypeMaps = false;
                //c.CreateMap<SampleList<T>, SampleListDTO<T>>()
                c.CreateMap(typeof(SampleList<>), typeof(SampleListDTO<>));
            });


            var sampleList = new SampleList<int>();
            sampleList.Value = 10001;
            sampleList.Name = "My Name";

            var sampleListDTO = Mapper.Map<SampleList<int>, SampleListDTO<int>>(sampleList);

            Console.WriteLine("Name : " + sampleList.Name);

            var sampleList1 = new SampleList<string>();
            sampleList1.Value = "My String";
            sampleList1.Name = "My Name 1";

            var sampleListDTO1 = Mapper.Map<SampleList<string>, SampleListDTO<string>>(sampleList1);


            Console.WriteLine("Name : " + sampleList1.Name);
        }
    }
}
