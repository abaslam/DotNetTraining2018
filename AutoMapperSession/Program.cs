﻿using Automapper2018.Topics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automapper2018
{
    class Program
    {
        static void Main(string[] args)
        {
            //HandCodedMapper.Map();

            //AutoMapperBasics.SimpleMap();
            //AutoMapperBasics.MapFrom();
            //AutoMapperBasics.ReverseMap();
            //AutoMapperBasics.Ignore();
            //AutoMapperBasics.ConditionalMapping();
            //AutoMapperBasics.GenericMapping();
            //AutoMapperBasics.OpenGenericMapping();

            //AutoMapperAdvanced.Validation();
            //AutoMapperAdvanced.NullSubstitution();
            //AutoMapperAdvanced.ConstructUsing1();
            //AutoMapperAdvanced.ConstructUsing2();

            //AutoMapperAdvanced.TypeConvertor();
            //AutoMapperAdvanced.BeforeAfterMap1();
            //AutoMapperAdvanced.BeforeAfterMap2();
            AutoMapperAdvanced.MappingConfigurationSplit();
            Console.ReadLine();
        }
    }
}
