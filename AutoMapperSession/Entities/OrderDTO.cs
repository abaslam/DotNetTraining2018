﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automapper2018.Entities
{
    public class OrderDTO
    {
        public string CustomerName { get; set; }
        public decimal Total { get; set; }
    }
}
